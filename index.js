class Calculadora{

    constructor (a, b){
        this.numero=a;
        this.grados=b;
    }

    calcular (){
        this.gradosARadianes ();
        this.radianesAGrados ();
        this.kelvinACentigrados ();
        this.centigradosAKelvin ();
    }
}
class CalculadoraConvertidora extends Calculadora{
    
    gradosARadianes () {

        let angulo=(this.numero)*Math.pi/180;
        return angulo;
       
    }
    radianesAGrados(){

        let angulo= (this.numero) *Math.pi*180;
        return angulo;
        

    }
    kelvinACentigrados(){

        let conversor=(this.grados)-273;
        return conversor;
        

    }
    centigradosAKelvin (){

        let conversor=(this.grados)+273;
        return conversor;
        


    }

}
let miConversion = new CalculadoraConvertidora (70,15); 

